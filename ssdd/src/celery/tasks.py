from celery import Celery

app = Celery('tasks', backend="amqp", broker='pyamqp://guest@localhost//')

@app.task
def add(x, y):
    print x, y
    return x + y
